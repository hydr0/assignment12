package assignment12;

import static org.junit.Assert.*;
import java.io.*;
import java.util.*;

import org.junit.Before;
import org.junit.Test;

public class TestHuffmanTree {
	private static Scanner a;
	private static Scanner b;
	
	@Before
	public void setUp() throws FileNotFoundException{
		CompressionDemo.compressFile("Dantes Inferno Canto 1.txt", "DIcompressed.txt");
		CompressionDemo.decompressFile("DIcompressed.txt", "DIdecompressed.txt");
		
		a = new Scanner(new File("Dantes Inferno Canto 1.txt"));
		b = new Scanner(new File("DIdecompressed.txt"));
		
	}

	@Test
	public void testCompress(){
		
		boolean match = true;
		while (a.hasNext()){
			String aLine = a.nextLine();
			String bLine = b.nextLine();
			for (int i = 0; i < aLine.length(); i++){
				if (!(aLine.charAt(i) == bLine.charAt(i))){
					match = false;
					System.out.println("Mismatch @ " + i + "in line: " + bLine);
				}
			}
		}
		
		assertTrue(match);
	}
	
	@Test
	public void testCode() {
		
	}

}
