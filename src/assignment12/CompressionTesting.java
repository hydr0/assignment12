
package assignment12;

public class CompressionTesting {

	// The original files to be read and compressed
	public static String[] ORIGINAL = {"The Divine Comedy.txt","Bram Stroker's Dracula.txt",
		"Dantes Inferno Canto 1.txt","test5kchar.txt",
		"testpalindromes.txt","15chars.txt"};
	// Decompressed file names
	public static String[] DECOMPF = {"DivComDeco.txt","DracDeco.txt",
		"DIC1Deco.txt","5kDeco.txt",
		"palDeco.txt","15Deco.txt"};
	// Compressed file names
	public static String[] COMPF= {"DivComComp.txt","DracComp.txt",
		"DIC1Comp.txt","5kComp.txt",
		"palComp.txt","15Comp.txt"};
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (int i = 0; i < ORIGINAL.length; i++) {
			System.out.println("Compressing " + ORIGINAL[i] + "...");
			CompressionDemo.compressFile(ORIGINAL[i], COMPF[i]);
			System.out.println("Done!\nCompressed file name is: " +
					COMPF[i] + "\n\nDecompressing " + COMPF[i] + "...");
			CompressionDemo.decompressFile(COMPF[i], DECOMPF[i]);
			System.out.println("Done!\nDecompressed file name is: " +
					DECOMPF[i] + "\n");
		}
	}

}
